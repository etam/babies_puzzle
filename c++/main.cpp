#include <algorithm>
#include <array>
#include <iostream>
#include <parallel/algorithm>
#include <stdexcept>
#include <vector>

#define REP(i, N) for (int i = 0; i < N; ++i)
#define ALL(v) std::begin(v), std::end(v)

typedef unsigned char byte;


/*
 * Baby card
 *
 * base position:
 *  h
 * l h
 *  l
 * where h is head and l is legs
 *
 * positions in card:
 *  0
 * 3 1
 *  2
 *
 * angles:
 *      A0
 * A270    A90
 *     A180
 * rotation by angle means the field is on top
 */

enum class Color : byte { B, G, R, Y };
typedef std::array<Color, 4> Card;
typedef std::array<Card, 9> CardsPerm;


template <typename T>
constexpr
T factorial(T n)
{
    auto acc = T{1};
    while (n > 1) {
        acc *= n--;
    }
    return acc;
}


template <typename Container>
std::vector<Container> allPermutations(Container container)
{
    auto result = std::vector<Container>{};
    result.reserve(factorial<std::size_t>(container.size()));
    do {
        result.push_back(container);
    } while (std::next_permutation(ALL(container)));
    return result;
}


enum Angle : byte { A0 = 0, A90 = 1, A180 = 2, A270 = 3 };
typedef std::array<Angle, 9> Angles;


bool anglesMatch(const Angles& angles)
{
    REP (y, 3) {
        REP (x, 2) {
            const auto angleA = angles[y*3 + x];
            const auto cardAHasLegsOnRight = static_cast<bool>(((angleA + 1) % 4) / 2);

            const auto angleB = angles[y*3 + x+1];
            const auto cardBHasHeadOnLeft = static_cast<bool>(((angleB + 1) % 4) / 2);

            if (cardAHasLegsOnRight != cardBHasHeadOnLeft) {
                return false;
            }
        }
    }
    REP (y, 2) {
        REP (x, 3) {
            const auto angleA = angles[y*3 + x];
            const auto cardAHasLegsOnBottom = static_cast<bool>(angleA / 2);

            const auto angleB = angles[(y+1)*3 + x];
            const auto cardBHasHeadOnTop = static_cast<bool>(angleB / 2);

            if (cardAHasLegsOnBottom != cardBHasHeadOnTop) {
                return false;
            }
        }
    }
    return true;
}


std::vector<Angles> allRotations(const CardsPerm& cardsPermutation, const Card card0)
{
    auto angless = std::vector<Angles>{};
    angless.reserve(factorial<std::size_t>(9));

    auto angles = Angles{};
    for (auto carry = false; !carry; ) {
        carry = true;
        REP (i, 9) {
            if (!carry) {
                break;
            }
            if (cardsPermutation[i] == card0) {
                // Rotating all cards by the same angle is identical to not
                // rotating them at all, but looking at rotated table.
                // By not rotating a single card we avoid checking those
                // 'rotated table' solutions.
                continue;
            }

            angles[i] = static_cast<Angle>((angles[i]+1) % 4);
            carry = (angles[i] == A0);
        }

        if (anglesMatch(angles)) {
            angless.push_back(angles);
        }
    }

    return angless;
}


constexpr
bool colorsMatch(const CardsPerm& cards, const Angles& angles)
{
    REP (y, 3) {
        REP (x, 2) {
            const auto iA = y*3 + x;
            const auto angleA = angles[iA];
            const auto cardA = cards[iA];
            const auto colorAOnRight = cardA[(angleA+1) % 4];

            const auto iB = iA + 1;
            const auto angleB = angles[iB];
            const auto cardB = cards[iB];
            const auto colorBOnLeft = cardB[(angleB+3) % 4];

            if (colorAOnRight != colorBOnLeft) {
                return false;
            }
        }
    }
    REP (y, 2) {
        REP (x, 3) {
            const auto iA = y*3 + x;
            const auto angleA = angles[iA];
            const auto cardA = cards[iA];
            const auto colorAOnBottom = cardA[(angleA+2) % 4];

            const auto iB = iA + 3;
            const auto angleB = angles[iB];
            const auto cardB = cards[iB];
            const auto colorBOnTop = cardB[angleB];

            if (colorAOnBottom != colorBOnTop) {
                return false;
            }
        }
    }
    return true;
}


std::ostream& operator<<(std::ostream& o, Color color)
{
    switch (color) {
    case Color::B: return o << 'B';
    case Color::G: return o << 'G';
    case Color::R: return o << 'R';
    case Color::Y: return o << 'Y';
    default: throw std::runtime_error{"invalid color"};
    }
}

Card rotated(Card card, Angle angle)
{
    return {{
        card[angle],
        card[(angle+1) % 4],
        card[(angle+2) % 4],
        card[(angle+3) % 4],
    }};
}

CardsPerm rotated(CardsPerm cardsPerm, const Angles& angles)
{
    REP (i, 9) {
        cardsPerm[i] = rotated(cardsPerm[i], angles[i]);
    }
    return cardsPerm;
}


std::ostream& operator<<(std::ostream& o, const std::pair<CardsPerm, Angles>& result)
{
    const auto rotatedCardsPerm = rotated(result.first, result.second);
    REP (y, 3) {
        REP (x, 3) {
            o << ' ' << rotatedCardsPerm[y*3+x][0] << ' ';
        }
        o << '\n';

        REP (x, 3) {
            o << rotatedCardsPerm[y*3+x][3] << ' ' << rotatedCardsPerm[y*3+x][1];
        }
        o << '\n';

        REP (x, 3) {
            o << ' ' << rotatedCardsPerm[y*3+x][2] << ' ';
        }
        o << '\n';
    }
    return o;
}


int main()
{
    const auto cards = CardsPerm{{
        Card({{Color::G, Color::R, Color::G, Color::B}}),
        Card({{Color::G, Color::R, Color::Y, Color::B}}),
        Card({{Color::G, Color::R, Color::Y, Color::B}}),
        Card({{Color::G, Color::Y, Color::B, Color::R}}),
        Card({{Color::G, Color::Y, Color::R, Color::Y}}),
        Card({{Color::Y, Color::B, Color::G, Color::R}}),
        Card({{Color::Y, Color::B, Color::Y, Color::R}}),
        Card({{Color::Y, Color::G, Color::R, Color::Y}}),
        Card({{Color::Y, Color::R, Color::G, Color::B}}),
    }};

    const auto cardsPermutations = allPermutations(cards);

    auto results = std::vector<std::pair<CardsPerm, Angles>>{};

    std::__parallel::for_each(ALL(cardsPermutations),
                              [&results, card0 = cards[0]](const auto& cardsPermutation) {
        for (const auto& rotation : allRotations(cardsPermutation, card0)) {
            if (colorsMatch(cardsPermutation, rotation)) {
                #pragma omp critical
                results.push_back({cardsPermutation, rotation});
            }
        }
    });

    for (const auto& result : results) {
        std::cout << result << '\n';
    }
}
