#include <array>
#include <cstdlib>
#include <future>
#include <iostream>
#include <thread>
#include <utility>
#include <vector>

#include <range/v3/all.hpp>

#define REP(i, N) for (int i = 0; i < N; ++i)

typedef unsigned char byte;


/*
 * Baby card
 *
 * base position:
 *  h
 * l h
 *  l
 * where h is head and l is legs
 *
 * positions in card:
 *  0
 * 3 1
 *  2
 *
 * angles:
 *      A0
 * A270    A90
 *     A180
 * rotation by angle means the field is on top
 */

enum class Color : byte { B, G, R, Y };
typedef std::array<Color, 4> Card;
typedef std::array<Card, 9> CardsPerm;

enum Angle : byte { A0 = 0, A90 = 1, A180 = 2, A270 = 3 };
typedef std::array<Angle, 9> Angles;


bool anglesMatch(const Angles& angles)
{
    REP (y, 3) {
        REP (x, 2) {
            const auto angleA = angles[y*3 + x];
            const auto cardAHasLegsOnRight = static_cast<bool>(((angleA + 1) % 4) / 2);

            const auto angleB = angles[y*3 + x+1];
            const auto cardBHasHeadOnLeft = static_cast<bool>(((angleB + 1) % 4) / 2);

            if (cardAHasLegsOnRight != cardBHasHeadOnLeft) {
                return false;
            }
        }
    }
    REP (y, 2) {
        REP (x, 3) {
            const auto angleA = angles[y*3 + x];
            const auto cardAHasLegsOnBottom = static_cast<bool>(angleA / 2);

            const auto angleB = angles[(y+1)*3 + x];
            const auto cardBHasHeadOnTop = static_cast<bool>(angleB / 2);

            if (cardAHasLegsOnBottom != cardBHasHeadOnTop) {
                return false;
            }
        }
    }
    return true;
}


class AllRotations
    : public ranges::view_facade<AllRotations>
{
private:
    friend struct ranges::range_access;
    const CardsPerm * mCardsPermutation;
    Card mCard0;
    Angles mAngles = {};
    bool mFinished = false;

public:
    AllRotations() = default;

    AllRotations(const CardsPerm& cardsPermutation, const Card card0)
        : mCardsPermutation{&cardsPermutation}
        , mCard0{std::move(card0)}
    {}

    const Angles& read() const
    {
        return mAngles;
    }

    bool equal(ranges::default_sentinel) const
    {
        return mFinished;
    }

    void next()
    {
        bool carry = true;
        REP (i, 9) {
            if (!carry) {
                break;
            }
            if ((*mCardsPermutation)[i] == mCard0) {
                // Rotating all cards by the same angle is identical to not
                // rotating them at all, but looking at rotated table.
                // By not rotating a single card we avoid checking those
                // 'rotated table' solutions.
                continue;
            }
            mAngles[i] = static_cast<Angle>((mAngles[i]+1) % 4);
            carry = (mAngles[i] == A0);
        }
        mFinished = carry;
    }
};


constexpr
bool colorsMatch(const CardsPerm& cards, const Angles& angles)
{
    REP (y, 3) {
        REP (x, 2) {
            const auto iA = y*3 + x;
            const auto angleA = angles[iA];
            const auto cardA = cards[iA];
            const auto colorAOnRight = cardA[(angleA+1) % 4];

            const auto iB = iA + 1;
            const auto angleB = angles[iB];
            const auto cardB = cards[iB];
            const auto colorBOnLeft = cardB[(angleB+3) % 4];

            if (colorAOnRight != colorBOnLeft) {
                return false;
            }
        }
    }
    REP (y, 2) {
        REP (x, 3) {
            const auto iA = y*3 + x;
            const auto angleA = angles[iA];
            const auto cardA = cards[iA];
            const auto colorAOnBottom = cardA[(angleA+2) % 4];

            const auto iB = iA + 3;
            const auto angleB = angles[iB];
            const auto cardB = cards[iB];
            const auto colorBOnTop = cardB[angleB];

            if (colorAOnBottom != colorBOnTop) {
                return false;
            }
        }
    }
    return true;
}


template <typename Range>
class AllPermutations
    : public ranges::view_facade<AllPermutations<Range>>
{
private:
    friend struct ranges::range_access;
    Range mRange;
    bool mIsLast = false;

    const Range& read() const
    {
        return mRange;
    }

    bool equal(ranges::default_sentinel) const
    {
        return mIsLast;
    }

    void next()
    {
        mIsLast = !ranges::next_permutation(mRange);
    }

public:
    AllPermutations() = default;

    explicit
    AllPermutations(Range range)
        : mRange{std::move(range) | ranges::action::sort }
    {}
};

template <typename Range>
auto allPermutations(Range range)
{
    return AllPermutations<Range>{std::move(range)};
}


auto divideLengthIntoOffsetsAndLengths(const int n, const int m)
{
    const auto div = std::div(n, m);
    const auto lengths = ranges::view::concat(
        ranges::view::repeat_n(div.quot+1, div.rem),
        ranges::view::repeat_n(div.quot, m-div.rem)
    );
    const auto offsets = ranges::view::concat(
        ranges::view::single(0),
        lengths | ranges::view::partial_sum()
    );
    return ranges::view::zip(offsets, lengths);
}

auto chunks(const int m)
{
    return ranges::make_pipeable([m](const auto& rng) {
        const auto offsetsAndLengths = divideLengthIntoOffsetsAndLengths(rng.size(), m);
        return offsetsAndLengths | ranges::view::transform([&rng](const auto offsetAndLength) {
            return rng
                | ranges::view::drop_exactly(offsetAndLength.first)
                | ranges::view::take_exactly(offsetAndLength.second);
        });
    });
}


template <typename View>
auto parallel(View view)
{
    return ranges::make_pipeable([view](const auto& rng) {
        auto futures =
            rng
            | chunks(std::thread::hardware_concurrency())
            | ranges::view::transform([view](auto chunk_view) {
                return std::async(std::launch::async, [=] {
                    return chunk_view | view | ranges::to_vector;
                });
            })
            | ranges::to_vector;
        return
            futures
            | ranges::view::transform([](auto& future) {
                return future.get();
            })
            | ranges::action::join;
    });
}

template <typename View>
auto not_parallel(View view)
{
    return view | ranges::to_vector;
}


std::ostream& operator<<(std::ostream& o, Color color)
{
    switch (color) {
    case Color::B: return o << 'B';
    case Color::G: return o << 'G';
    case Color::R: return o << 'R';
    case Color::Y: return o << 'Y';
    default: throw std::runtime_error{"invalid color"};
    }
}


Card rotated(Card card, Angle angle)
{
    return {{
        card[angle],
        card[(angle+1) % 4],
        card[(angle+2) % 4],
        card[(angle+3) % 4],
    }};
}


auto toSinglePairs = ranges::make_pipeable([](const std::pair<auto, auto>& input)
{
    return ranges::view::zip(input.first, input.second);
});


/*
 * TODO: This could be reimplemented with ranges, using ideas from
 * range-v3/example/calendar.cpp, but I don't think it's worth the effort.
 */
std::ostream& operator<<(std::ostream& o, const std::pair<CardsPerm, Angles>& result)
{
    const auto rotatedCardsPerm =
        result
        | toSinglePairs
        | ranges::view::transform([](const std::pair<Card, Angle> input) {
            return rotated(input.first, input.second);
        })
        | ranges::to_vector;
    REP (y, 3) {
        REP (x, 3) {
            o << ' ' << rotatedCardsPerm[y*3+x][0] << ' ';
        }
        o << '\n';

        REP (x, 3) {
            o << rotatedCardsPerm[y*3+x][3] << ' ' << rotatedCardsPerm[y*3+x][1];
        }
        o << '\n';

        REP (x, 3) {
            o << ' ' << rotatedCardsPerm[y*3+x][2] << ' ';
        }
        o << '\n';
    }
    return o;
}


int main()
{
    const auto cards = CardsPerm{{
        Card({{Color::G, Color::R, Color::G, Color::B}}),
        Card({{Color::G, Color::R, Color::Y, Color::B}}),
        Card({{Color::G, Color::R, Color::Y, Color::B}}),
        Card({{Color::G, Color::Y, Color::B, Color::R}}),
        Card({{Color::G, Color::Y, Color::R, Color::Y}}),
        Card({{Color::Y, Color::B, Color::G, Color::R}}),
        Card({{Color::Y, Color::B, Color::Y, Color::R}}),
        Card({{Color::Y, Color::G, Color::R, Color::Y}}),
        Card({{Color::Y, Color::R, Color::G, Color::B}}),
    }};

    const auto cardsPermutations = allPermutations(cards) | ranges::to_vector;

    const auto results =
        cardsPermutations
        | parallel(ranges::view::for_each([card0 = cards[0]](const CardsPerm& cardsPermutation) {
            return
                AllRotations(cardsPermutation, card0)
                | ranges::view::filter(anglesMatch)
                | ranges::view::filter([&cardsPermutation](const Angles& angles) {
                    return colorsMatch(cardsPermutation, angles);
                })
                | ranges::view::transform([&cardsPermutation](const Angles& angles) {
                    return std::make_pair(cardsPermutation, angles);
                });
        }));

    for (const auto& result : results) {
        std::cout << result << '\n';
    }
}
