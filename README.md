Babies
======

Goal of this puzzle is to put those 9 cards

![cards](img/all.jpg)

into an 3 by 3 square so that babies (except the half-babies on the outer edge) consists of one head, two hands and two legs and has single color body.

There are exactly 2 solutions: [1](img/1.jpg) and [2](img/2.jpg).

These implementations finds those solutions:

- [c++/main.cpp](c++/main.cpp): C++ with [gcc's parallel algorithms](https://gcc.gnu.org/onlinedocs/libstdc++/manual/parallel_mode.html) extension
- [c++-ranges/main.cpp](c++-ranges/main.cpp): C++ using [range-v3](https://github.com/ericniebler/range-v3) with hand-made parallelism

The code is released under the terms of [WTFPL](http://www.wtfpl.net/).
